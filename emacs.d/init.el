(tool-bar-mode -1)
(toggle-scroll-bar -1) 
(setq inhibit-startup-screen t)
(setq-default indent-tabs-mode nil)

(find-file "/Users/mukundaa/.emacs.d/init.el")
;(find-file "/Users/mukundaa/work.org")

(require 'package)

(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(require 'diminish)
(require 'bind-key)

;;; Installed packages

(use-package evil
  :ensure t
  :init
  (setq evil-want-C-u-scroll t)
  :config
  (evil-mode +1))

(use-package helm
  :ensure t
  :bind (("M-x" . helm-M-x)
         ("M-<f5>" . helm-find-files)
         ([f10] . helm-buffers-list)
         ([S-f10] . helm-recentf))
  :init
  (define-key evil-ex-map "e" 'helm-find-files))

(use-package neotree
  :ensure t
  :bind (("C-q" . neotree-toggle))
  :config
  (evil-define-key 'normal neotree-mode-map (kbd "TAB") 'neotree-enter)
  (evil-define-key 'normal neotree-mode-map (kbd "SPC") 'neotree-quick-look)
  (evil-define-key 'normal neotree-mode-map (kbd "q") 'neotree-hide)
  (evil-define-key 'normal neotree-mode-map (kbd "RET") 'neotree-enter))
(setq neo-smart-open t)

;; Theme
(use-package all-the-icons
  :ensure t)
(use-package doom-themes
  :ensure t)
(load-theme 'doom-one t)
(doom-themes-visual-bell-config)
(doom-themes-neotree-config)  ; all-the-icons fonts must be installed!
(doom-themes-org-config)

;; Javascript
(use-package js2-mode
  :ensure t
  :defer)

(use-package js2-refactor
  :ensure t
  :defer)

(use-package xref-js2
  :ensure t
  :defer)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (doom-themes all-the-icons xref-js2 use-package try neotree js2-refactor helm evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
